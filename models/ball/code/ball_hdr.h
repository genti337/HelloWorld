
/** TRICK HEADER **************************************************************
@file

@verbatim
PURPOSE:
    (Ball model EOM state parameter definition.)
ASSUMPTIONS AND LIMITATIONS:
    ((2 dimensional space)
     (Translational EOM only))
PROGRAMMERS:
    (((Your Name) (Company Name) (Date) (Trick tutorial)))
@endverbatim
*******************************************************************************/


#ifndef BALL_HDR_H
#define BALL_HDR_H

/** @struct BSTATE_IN
    @brief ball state input parameters
 */

//#include "ball_state.h"

//struct DATA { /* BSTATE_IN ------------------------------------------------*/
//  double mass ; /* ** */  
//}; /*--------------------------------------------------------------*/
typedef struct DATA MY_DATA;

//typedef DATA MY_DATA;

#endif
